class PropertyXML::XMLProperty
  def initialize(details = {})
    @property = details
  end
  def reaxml_time_format time
    time.strftime "%Y-%m-%d-%H:%M:%S"
  end
  def reaxml_status property
    if property[:sold]
      'sold'
    elsif property[:withdrawn]
      'withdrawn'
    else
      'current'
    end
  end

  def to_reaxml xml
    # Start a property XML
    xml.residential :modTime => reaxml_time_format(@property[:updated_at]), :status => reaxml_status(@property) do
      # These must always be here
      xml.agentID @property[:agent_id]
      xml.uniqueID @property[:id]
      # If the property is sold, we only need to list the sale details
      if @property[:sold]
          xml.soldDetails do
          xml.price @property[:sale_price], :display => "yes"
          xml.date reaxml_time_format(@property[:sale_time])
        end
      # If the property is withdrawn, we don't want to list any details.  Otherwise, it's a current listing.
      elsif not @property[:withdrawn]
        xml.authority :value => "#{@property[:auction] ? 'auction' : 'exclusive'}"
        xml.underOffer :value => "#{@property[:under_offer] ? 'yes' : 'no'}"
        xml.listingAgent do
          xml.name @property[:agent_name]
          xml.telephone @property[:agent_phone], :type => "BH"
          xml.email @property[:agent_email]
        end
        xml.price @property[:price], :display => "yes"
        xml.priceView @property[:price_text]
        xml.address :display => "yes" do
          # Optional
          xml.subNumber @property[:unit_number]

          # Required
          xml.streetNumber @property[:street_number]
          xml.street @property[:street]
          xml.suburb @property[:suburb], :display => "yes"
          xml.state @property[:state]
          xml.postcode @property[:postcode]
        end
        xml.category :name => @property[:category]
        xml.headline @property[:headline]
        xml.description @property[:description]
        xml.features do
          # Required
          xml.bedrooms @property[:bedrooms]
          xml.bathrooms @property[:bathrooms]

          # Optional
          xml.ensuite @property[:ensuites]
          xml.garages @property[:garages]
          xml.carports @property[:carports]
        end
        xml.landDetails do
          xml.area @property[:land_area], :unit => "square"
        end
        xml.buildingDetails do
          xml.area @property[:floor_area], :unit => "square"
          # Required
          xml.energyRating @property[:eer]
        end
        unless @property[:exhibitions].nil? or @property[:exhibitions].blank?
          xml.inspectionTimes do
            @property[:exhibitions].each do |exhibition|
              xml.inspection "#{exhibition[:start_time].strftime("%d-%b-%Y %l:%M%P")} to #{exhibition[:finish_time].strftime("%l:%M%P")}"
            end
          end
        end
        if @property[:auction]
          xml.auction :date => @property[:auction_time].strftime("%Y-%m-%dT%H:%M:%S%z")
        end
        xml.vendorDetails do
          xml.name @property[:vendor_name]
          xml.telephone @property[:vendor_phone], :type => "BH"
        end
        if @property[:vendor_name]
          xml.vendorDetails do
            xml.name @property[:vendor_name]
            xml.telephone @property[:vendor_phone], :type => "BH"
          end
        end
        xml.externalLink :href => @property[:url]
        unless @property[:images].nil? or @property[:images].blank?
          xml.images do
            @property[:images][0..26].each_with_index do |image, index|
              index == 0 ? id = 'm' : id = (index+64).chr.downcase
              xml.img :id => id, :modTime => image[:updated_at], :url => image[:url], :format => "jpg"
            end
          end
        end
      end
    end
  end
end