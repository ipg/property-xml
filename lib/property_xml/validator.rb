class PropertyXML::Validator
  REAXML_DTD_PATH = "http://reaxml.realestate.com.au/propertyList.dtd"

  def self.validate xml_file
    require 'libxml'
    require 'open-uri'
    include LibXML
 
    dtd = XML::Dtd.new(open(REAXML_DTD_PATH).read)
    doc = XML::Document.file(xml_file)
    doc.validate(dtd)
  end

end