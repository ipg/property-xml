class PropertyXML
  require 'builder'
  require "property_xml/version"
  require "property_xml/validator"
  require "property_xml/xml_property"
  require "property_xml/xml_rental"

  def initialize opts={}
    @properties = {}
    @reaxml = {:username => opts[:reaxml_username], :password => opts[:reaxml_password]}
    @xml = Builder::XmlMarkup.new :indent => 2
    @xml.instruct!
    @xml.declare! :DOCTYPE, :propertyList, :SYSTEM, "http://reaxml.realestate.com.au/propertyList.dtd"
  end

  def add_property property, type = :residential
    @properties[type] ||= []
    @properties[type] << property
  end

  def generate
    @xml.propertyList :date => Time.now.strftime("%Y-%m-%d-%H:%M:%S"), :username => @reaxml[:username], :password => @reaxml[:password] do
      @properties.each_pair do |type, property_array|
        property_array.each do |property|
          property.to_reaxml @xml
        end
      end
    end
  end
end
