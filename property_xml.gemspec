# -*- encoding: utf-8 -*-
require File.expand_path('../lib/property_xml/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Daniel Cox"]
  gem.email         = ["dtc@independent.com.au"]
  gem.description   = %q{An application agnostic generator for REAXML, just give it a hash of your property's detais and it'll give you an XML in return}
  gem.summary       = %q{REAXML XML generator}
  gem.homepage      = "http://bitbucket.org/ipg/property-xml"

  gem.files         = `git ls-files`.split($\)
  # gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  # gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "property_xml"
  gem.require_paths = ["lib"]
  gem.version       = PropertyXML::VERSION
  gem.add_dependency("builder")
  gem.add_dependency("libxml-ruby")
end
