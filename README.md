# Property XML

An application agnostic generator for REAXML, just give it a hash of your property's detais and it'll give you an XML in return

## Installation

Add this line to your application's Gemfile:

    gem 'property_xml', :git => "git@bitbucket.org:ipg/property-xml.git"

And then execute:

    $ bundle

## Usage

Start by creating a new instance of the PropertyXML class with your details:

    xml_generator = PropertyXML.new :reaxml_username => "username", :reaxml_password => "password"

Now for each property, do the following:

    # Create a set of images for the property
    images = [
      {:id => 1, :updated_at => Time.now, :url => "http://yoursite.com/image_1.jpg"},
      {:id => 2, :updated_at => Time.now, :url => "http://yoursite.com/image_2.jpg"}
    ]
    
    # Create exhibitions
    exhibitions = [
      {:start_time => 1.hour.ago, :finish_time => Time.now},
      (:start_time => 2.days.from_now, :finish_time => 3.days.from_now)
    ]

    # Create the property itself, referencing the images and exhibitions variables created above
    property = PropertyXML::XMLProperty.new(
      :updated_at => Time.now,
      :category => "House",
      :price => 500000,
      :headline => "Outstanding Property",
      :description => "This amazing property...",
      :unit_number => 10,
      :street_number => 20,
      :street => "John Smith Crescent",
      :suburb => "Hughes",
      :state => "ACT",
      :postcode => 2605,
      :images => images
      :exhibitions => exhibitions
    ))

Once you've created a property, add it to the generator:

    xml_generator.add_property(property)

After you've added all properties to the generator, generate the XML:

    xml = xml_generator.generate

Finally, you need to validate the generated XML against the REAXML.  Please note the PropertyXML gem **does not check the XML until this point**, so don't rely on it to throw errors if you miss a required field or provide incorrect data.

    # Returns true if all ok, otherwise with throw exceptions with errors
    PropertyXML::Validator.validate xml

## Valid Options and Data Types

### Property

These are the valid hash keys for a new instance of PropertyXML::Property.  For more info, please see the corresponding DTD.

      :sold # Boolean
      :agent_id # Integer or String
      :id # Integer or String
      :sale_price # Integer or String
      :sale_time # Time or DateTime
      :withdrawn # Boolean
      :category # String - House, Unit, Townhouse, Villa, Apartment, Flat, Studio, Warehouse, DuplexSemi-detached, Retirement, BlockOfUnits, Terrace, ServicedApartment, Other
      :price # Integer or String
      :headline # String
      :description # String
      :auction # Boolean
      :auction_time # Time or DateTime
      :under_offer # Boolean
      :url # String
      :price_text # String
      :unit_number # Integer or String
      :street_number # Integer or String
      :street # String
      :suburb # String
      :state # String
      :postcode # Integer or String
      :images # Array - See doc above
      :exhibitions # Array - See doc above
      :floor_area # Float or String
      :eer # Float or String
      :bedrooms # Integer or String
      :bathrooms # Integer or String
      :ensuites # Integer or String
      :garages # Integer or String
      :carports # Integer or String
      :agent_name # String
      :agent_phone # String
      :agent_email # String
      :land_area # Float or String
      :vendor_name # String
      :vendor_phone # String
      :updated_at # Time or DateTime

### Images

      :id # Integer or String
      :updated_at # Time or DateTime
      :url # String

### Exhibitions

      :start_time # Time or DateTime
      :finish_time # Time or DateTime

### Rental

These are the valid hash keys for a new instance of PropertyXML::Rental.  For more info, please see the corresponding DTD.

      :rented # Boolean
      :agent_id # Integer or String
      :id # Integer or String
      :withdrawn # Boolean
      :category # String - House, Unit, Townhouse, Villa, Apartment, Flat, Studio, Warehouse, DuplexSemi-detached, Retirement, BlockOfUnits, Terrace, ServicedApartment, Other
      :rent # Integer or String
      :headline # String
      :description # String
      :url # String
      :price_text # String
      :unit_number # Integer or String
      :street_number # Integer or String
      :street # String
      :suburb # String
      :state # String
      :postcode # Integer or String
      :images # Array - See doc above
      :exhibitions # Array - See doc above
      :floor_area # Float or String
      :eer # Float or String
      :bedrooms # Integer or String
      :bathrooms # Integer or String
      :ensuites # Integer or String
      :garages # Integer or String
      :carports # Integer or String
      :agent_name # String
      :agent_phone # String
      :agent_email # String
      :land_area # Float or String
      :updated_at # Time or DateTime

## Limitations

Some things are currently hardcoded for simplicity;

  + Only JPG images are supported

## Contributing

This gem was designed quickly to make generating XML feeds more efficient across multiple apps and as such is by no means comprehensive.  If you notice something missing / a flaw, please;

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
